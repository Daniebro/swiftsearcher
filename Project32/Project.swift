//
//  Project.swift
//  Project32
//
//  Created by Danni Brito on 6/2/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit

class Project: NSObject {
    var number: Int!
    var name: String!
    var topics: [String]!
    
    init(number: Int, name: String, topics: [String]) {
        self.name = name
        self.number = number
        self.topics = topics
    }
}
