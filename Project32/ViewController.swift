//
//  ViewController.swift
//  Project32
//
//  Created by Danni Brito on 4/15/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import CoreSpotlight
import MobileCoreServices
import SafariServices
import UIKit

class ViewController: UITableViewController {
    
    var projects = [Project]()
    var favourites = [Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        projects.append(Project(number: 1, name: "Storm Viewer", topics: ["Constants and variables", "UITableView", "UIImageView", "FileManager", "storyboards"]))
        projects.append(Project(number: 2, name: "Guess the Flag", topics: ["@2x and @3x images", "asset catalogs", "integers", "doubles", "floats", "operators (+= and -=)", "UIButton", "enums", "CALayer", "UIColor", "random numbers", "actions", "string interpolation", "UIAlertController"]))
        projects.append(Project(number: 3, name: "Social Media", topics: ["UIBarButtonItem", "UIActivityViewController", "the Social framework", "URL"]))
        projects.append(Project(number: 4, name: "Easy Browser", topics: ["loadView()", "WKWebView", "delegation", "classes and structs", "URLRequest", "UIToolbar", "UIProgressView", "key-value observing"]))
        projects.append(Project(number: 5, name: "Word Scramble", topics: ["Closures", "method return values", "booleans", "NSRange"]))
        projects.append(Project(number: 6, name: "Auto Layout", topics: ["Get to grips with Auto Layout using practical examples and code"]))
        projects.append(Project(number: 7, name: "Whitehouse Petitions", topics: ["JSON", "Data", "UITabBarController"]))
        projects.append(Project(number: 8, name: "Swifty Words", topics: ["addTarget()", "enumerated()", "count", "index(of:)", "property observers", "range operators."]))
        
        NotificationCenter.default.addObserver(self, selector: #selector(tableView.reloadData), name: UIContentSizeCategory.didChangeNotification, object: nil)
        
        let defaults = UserDefaults.standard
        if let savedFavourites = defaults.object(forKey: "favourites") as? [Int] {
            favourites = savedFavourites
        }
        
        tableView.isEditing = true
        tableView.allowsSelectionDuringEditing = true
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let project = projects[indexPath.row]
        cell.textLabel?.attributedText = makeAttributedString(title: "Project \(project.number ?? 0): \(project.name ?? "")", subtitle: project.topics.joined(separator: ", "))
        
        if favourites.contains(indexPath.row) {
            cell.editingAccessoryType = .checkmark
        } else {
            cell.editingAccessoryType = .none
        }
        
        return cell
    }
    
    func makeAttributedString(title: String, subtitle: String) -> NSAttributedString {
        let titleAttributes = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline), NSAttributedString.Key.foregroundColor: UIColor.purple]
        let subtitleAttributes = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .subheadline)]

        let titleString = NSMutableAttributedString(string: "\(title)\n", attributes: titleAttributes)
        let subtitleString = NSAttributedString(string: subtitle, attributes: subtitleAttributes)

        titleString.append(subtitleString)

        return titleString
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showTutorial(indexPath.row)
    }
    
    func showTutorial(_ which: Int) {
        if let url = URL(string: "https://www.hackingwithswift.com/read/\(which + 1)") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true

            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if favourites.contains(indexPath.row) {
            return .delete
        } else {
            return .insert
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .insert {
            favourites.append(indexPath.row)
            index(item: indexPath.row)
        } else {
            if let index = favourites.firstIndex(of: indexPath.row) {
                favourites.remove(at: index)
                deindex(item: indexPath.row)
            }
        }
        
        let defaults = UserDefaults.standard
        defaults.set(favourites, forKey: "favourites")
        
        tableView.reloadData()
    }
    
    func index(item: Int){
        let project = projects[item]
        
        let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeText as String)
        attributeSet.title = "Project \(project.number ?? 0): \(project.name ?? "")"
        attributeSet.contentDescription = project.topics.joined(separator: ", ")
        
        let item = CSSearchableItem(uniqueIdentifier: "\(item)", domainIdentifier: "com.danni", attributeSet: attributeSet)
        item.expirationDate = Date.distantFuture
        CSSearchableIndex.default().indexSearchableItems([item]) { (error) in
            if let error = error {
                print("Indexing error: \(error.localizedDescription)")
            } else {
                print("Search item succesfully indexed")
            }
        }
    }
    
    func deindex(item: Int){
        CSSearchableIndex.default().deleteSearchableItems(withIdentifiers: ["\(item)"]) { (error) in
            if let error = error {
                print("Deindexing error: \(error.localizedDescription)")
            } else {
                print("Search item succesfully removed")
            }
        }
    }
    

}

